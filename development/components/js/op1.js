export default class Op1 {
  constructor() {}
  scale_mesh(element) {
    element.computeWorldMatrix(true);
    var rotationX = element.rotation.x;
    var rotationY = element.rotation.y;
    var rotationZ = element.rotation.z;

    var matrixTx = BABYLON.Matrix.RotationX(rotationX);
    var matrixTy = BABYLON.Matrix.RotationY(rotationY);
    var matrixTz = BABYLON.Matrix.RotationZ(rotationZ);
    element.bakeTransformIntoVertices(matrixTx);
    element.bakeTransformIntoVertices(matrixTy);
    element.bakeTransformIntoVertices(matrixTz);

    element.computeWorldMatrix(true);
    element.rotation.x = 0;
    element.rotation.y = 0;
    element.rotation.z = 0;

    // element.computeWorldMatrix(true);
    // var scalingE = element.scaling;
    // var matrixS = BABYLON.Matrix.Scaling(scalingE.x, scalingE.y, scalingE.z);
    // element.bakeTransformIntoVertices(matrixS);
    // element.scaling.x=1;
    // element.scaling.y=1;
    // element.scaling.z=1;

    // element.material.alpha=0.8;
    // element.material.fillMode=1;

  }
  translate_mesh() {
    this.Mbase.computeWorldMatrix(true);
    var translatE = this.Mbase.getBoundingInfo().boundingSphere.centerWorld.clone();
    // var widthBase=this.Mbase.getBoundingInfo().boundingBox.extendSize.scale(2).x;
    // var new_scale=320/widthBase;
    this.meshes.forEach(element => {
      // element.scaling=new BABYLON.Vector3(new_scale,new_scale,new_scale);
      element.computeWorldMatrix(true);

      element.position.x -= translatE.x;
      element.position.y -= translatE.y;
      element.position.z -= translatE.z;

      element.position.y -= 7;

      // if (/building/.test(element.name) || /virtual/.test(element.name) || /Trayectoria/.test(element.name)) {
      if (/Trayectoria/.test(element.name)) {
        //   element.edgesWidth = 0.2;
        //   element.edgesColor = element.material.diffuseColor.toColor4(0.9);
        //   element.enableEdgesRendering();
        //   element.material.alpha = 0.8;
        // this.createBoxST(element);
        element.material.diffuseColor = new BABYLON.Color3(0.5, 0.5, 0.5);
        element.material.emissiveColor = new BABYLON.Color3(0.65, 0.65, 0.65);
      } else if (/block/.test(element.name)) {
        element.material.diffuseColor = new BABYLON.Color3(0.4, 0.4, 0.4);
        element.material.emissiveColor = new BABYLON.Color3(0.65, 0.65, 0.65);
      } else if (/base/.test(element.name)) {
        element.material.diffuseColor = new BABYLON.Color3(1, 1, 1);
        element.material.emissiveColor = new BABYLON.Color3(0.1, 0.1, 0.1);
      } else if (/river/.test(element.name)) {
        element.material.diffuseColor = new BABYLON.Color3(112 / 255, 188 / 255, 255 / 255);
        // element.material.emissiveColor = new BABYLON.Color3(0.1,0.1,0.1);
      }

    });

  }
  assign_parent(parent, child) {
    child.parent = parent;
    child.position.x -= parent.position.x;
    child.position.y -= parent.position.y;
    child.position.z -= parent.position.z;
    child.computeWorldMatrix(true);
    parent.computeWorldMatrix(true);

    child.initial_middle = Object.assign({}, child.getBoundingInfo().boundingSphere.centerWorld);
    child.initial_parent_pos = Object.assign({}, parent.position);
    // child.computeWorldMatrix(true);
  }
  getGroundCollision() {
    var size_base = this.Mbase.getBoundingInfo().boundingBox.extendSize.scale(2);
    var groundCollision = BABYLON.Mesh.CreateGround('cube0', size_base.x, size_base.z, 64, this.scene);

    groundCollision.visibility = 0;
    groundCollision.position = new BABYLON.Vector3(0, this.Mbase.position.y, 0);
    // groundCollision.position = new BABYLON.Vector3(0, -87, 0);
    groundCollision.isPickable = true;
    //groundCollision.receiveShadows = true;
    groundCollision.checkCollisions = true;
    groundCollision.useOctreeForCollisions = true;

    // groundCollision.physicsImpostor = new BABYLON.PhysicsImpostor(groundCollision, BABYLON.PhysicsImpostor.BoxImpostor, {
    //   mass: 0,
    //   // restitution: 0.9
    // }, this.scene);
    // this.groundCollision = groundCollision;
  }
  createIcons(reset) {
    if(reset===undefined)reset=0;

    // this.camera.position.z = -30;
    var _this = this;
    // this.createTextures();
    // this.createParticules();
    this.a_type_buildings = [];
    this.a_info_buildings = [];

    this.meshes.forEach(d => {
      var name = d.name.split("-")[0].trim();
      if (name === "game") {
        var type = d.name.split("-")[1];
        if (!_this.checkElementArray(_this.a_type_buildings, type)) {
          _this.a_buildings_icons[type] = [];
          _this.a_type_buildings.push(type);
        }
        d.computeWorldMatrix(true);
        _this.a_buildings_icons[type].push(d);
        // _this.createLinesDots(d,100);
        d.type = type;
      }
      if (name === "info") {
        var type = d.name.split("-")[1];
        if (!_this.checkElementArray(_this.a_info_buildings, type)) {
          _this.a_info_buildings[type] = [];
          // _this.a_type_buildings.push(type);
        }
        d.computeWorldMatrix(true);
        _this.a_info_buildings[type].push(d);
        d.type = type;
      }
    });

    this.a_quiz_marker = [];
    this.a_info_marker = [];
    this.a_lightColor_material = [];
    this.a_type_buildings.forEach(type => {
      // create building icons
      _this.createTextures(type);
      var a_meshes_icon = _this.a_buildings_icons[type];
      // _this.createLinesDots(a_meshes_icon, 100);
      var icon = _this.createIconBytype(type, a_meshes_icon);
      _this.a_quiz_marker.push(icon);
      _this.createParticules(icon, type);
      _this.lightColor(a_meshes_icon, type);

      // create info icons
      var a_meshes_icon_info = _this.a_info_buildings[type];
      _this.createTextures(type + "-info");
      _this.createLinesDots(a_meshes_icon_info, 100);
      var info_icon = _this.createIconsInfo(type + "-info", a_meshes_icon_info);
      _this.a_info_marker.push(info_icon);
      _this.createParticules(info_icon, type + "-info");

    });
    for (var i = 0; i < 5; i++) {
      _this.anim_travel_location.push(_this.a_path_info[i + 1]);
      _this.anim_travel_location.push(_this.a_path_drone[i]);
    }
    _this.anim_target_location = this.anim_travel_location[0];
  }
  a_colors() {
    return [
      {
        name: "health",
        color: "#ef4123"
      }, {
        name: "virtual",
        color: "#942170"
      }, {
        name: "environment",
        color: "#0a978d"
      }, {
        name: "transport",
        color: "#ffd400"
      }, {
        name: "security",
        color: "#5a9ddc"
      }
    ];
  }
  lightColor(a_mesh, type) {
    var middle = this.getMiddlePositionArrayMeshes(a_mesh);
    var middle_position_mesh = BABYLON.Mesh.Center(a_mesh);
    var shaderMaterial = new BABYLON.ShaderMaterial("beams" + a_mesh[0].name, this.scene, './shaders/beams', {

      // defines: 'asdsd',
      attributes: [
        "position", "uv"
      ],
      uniforms: [
        "worldViewProjection", "time", "baseColor", "highlight"
      ],
      needAlphaBlending: true
    });

    shaderMaterial.setFloat("time", 0);
    shaderMaterial.setFloat("highlight", 0);

    var map_color = d3.map(this.a_colors(), function(d) {
      return d.name;
    });
    var getObj = map_color.get(type);
    var hex_color = d3.color(getObj.color).rgb();

    shaderMaterial.setColor3("baseColor", new BABYLON.Color3(hex_color.r / 255, hex_color.g / 255, hex_color.b / 255));

    // shaderMaterial.setColor3("baseColor", BABYLON.Color3.Red());
    shaderMaterial.backFaceCulling = true;

    var lightColor_mesh = BABYLON.Mesh.CreatePlane("Building_beams_" + a_mesh[0].name, 1, this.scene);
    lightColor_mesh.billboardMode = BABYLON.Mesh.BILLBOARDMODE_Y;

    lightColor_mesh.material = shaderMaterial;
    // lightColor_mesh.parent = this.root_mesh;
    // lightColor_mesh.position = meshx.position.clone();

    var hight_height_mesh = 0;
    a_mesh.forEach(function(d) {
      d.computeWorldMatrix(true);
      var nh = d.getBoundingInfo().boundingBox.extendSize.scale(2).y;
      if (nh > hight_height_mesh) {
        hight_height_mesh = nh;
      }
    });
    // console.log(hight_height_mesh);
    lightColor_mesh.position.y = middle.y + hight_height_mesh + 1.2;

    lightColor_mesh.position.x = middle_position_mesh.x;
    lightColor_mesh.position.z = middle_position_mesh.z;

    lightColor_mesh.scaling.x = 0.5;
    lightColor_mesh.scaling.y = 2;
    this.a_lightColor_material.push(shaderMaterial);
    this.quizBeamGroup.push(lightColor_mesh);
  }
  createTextures(type) {
    var _this = this;
    // this.a_textures = {};
    // this.a_buildings.forEach(function(type){
    var tex = new BABYLON.Texture('images/smart-nation/icon-' + type + '.png', _this.scene);
    _this.a_textures[type] = tex;
    // });
  }
  createLinesDots(mesh, numbe_dots) {
    var middle_position_mesh2 = this.getMiddlePositionArrayMeshes(mesh);

    var name = mesh[0].name + "_sphere";
    var middle_position_mesh = BABYLON.Mesh.Center(mesh);
    // var mat = new BABYLON.StandardMaterial("matxx" + name, this.scene);
    var map_color = d3.map(this.a_colors(), function(d) {
      return d.name;
    });
    var getObj = map_color.get(mesh[0].type);
    var hex_color = d3.color(getObj.color).rgb();

    // mat.diffuseColor =
    // mat.color = BABYLON.Color3.Red();
    // mat.pointsCloud = true;
    // mat.pointSize = 10;

    var a_points = [];
    a_points.push(middle_position_mesh);
    var y_axis = 2.5 + middle_position_mesh2.y;
    a_points.push(new BABYLON.Vector3(middle_position_mesh.x, y_axis - 1, middle_position_mesh.z));
    a_points.push(new BABYLON.Vector3(middle_position_mesh.x, y_axis, middle_position_mesh.z));

    var lineHospital = BABYLON.MeshBuilder.CreateDashedLines("lineHospital" + name, {
      points: a_points,
      gapSize: 5,
      dashSize: 4,
      dashBn: 100,
      updatable: true
    }, this.scene);

    lineHospital.color = new BABYLON.Color3(hex_color.r / 255, hex_color.g / 255, hex_color.b / 255);
    this.lineDotsGroup.push(lineHospital);
    // console.log(middle_position_mesh.z);
    // console.log(mesh.position.z);

    // var low = this.getMiddlePositionArrayMeshes(mesh);
    //
    // var position_z = middle_position_mesh.z;
    // var position_x = middle_position_mesh.x;
    // // var position_y = middle_position_mesh.y + low.y;
    // var position_y = low.y;
    // var radio_circle = 0.05;
    // var CreateSphere_dot = BABYLON.Mesh.CreateSphere('sphere_dot'+name, 64, radio_circle, this.scene);
    // CreateSphere_dot.position.x = position_x;
    // CreateSphere_dot.position.z = position_z;
    // CreateSphere_dot.position.y = position_y;
    // CreateSphere_dot.material = mat;
    // for (var i = 1; i < numbe_dots; i++) {
    //   var ins = CreateSphere_dot.createInstance("instance_sphere"+name+ i);
    //   ins.position.x = position_x;
    //   ins.position.z = position_z;
    //   ins.position.y = position_y + (i * radio_circle);
    // }

  }

  createIconsInfo(type, mesh_position) {
    var _this = this;

    var marker = BABYLON.Mesh.CreatePlane("marker_" + type, 0.5, this.scene);
    marker.billboardMode = BABYLON.Mesh.BILLBOARDMODE_Y;

    // this.marker.parent = this.getPrimary().marker_mesh;
    var middle = this.getMiddlePositionArrayMeshes(mesh_position);
    var middle_position_mesh = BABYLON.Mesh.Center(mesh_position);

    // var hight_height_mesh=0;
    // mesh_position.forEach(function(d){
    //   d.computeWorldMatrix(true);
    //   var nh=d.getBoundingInfo().boundingBox.extendSize.scale(2).y;
    //   if(nh>hight_height_mesh){
    //     hight_height_mesh=nh;
    //   }
    // });

    marker.position.x = middle_position_mesh.x;
    // marker.position.y = middle_position_mesh.y+2;
    marker.offset = 2.45 + middle.y;
    marker.position.y = marker.offset;
    marker.position.z = middle_position_mesh.z;

    switch (type) {
      case "environment-info":
        this.a_path_info[1] = {
          x: middle_position_mesh.x,
          y: marker.offset,
          z: middle_position_mesh.z
        };
        break;
      case "health-info":
        this.a_path_info[2] = {
          x: middle_position_mesh.x,
          y: marker.offset,
          z: middle_position_mesh.z
        };
        break;
      case "virtual-info":
        this.a_path_info[3] = {
          x: middle_position_mesh.x,
          y: marker.offset,
          z: middle_position_mesh.z
        };
        break;
      case "transport-info":
        this.a_path_info[4] = {
          x: middle_position_mesh.x,
          y: marker.offset,
          z: middle_position_mesh.z
        };
        break;
      case "security-info":
        this.a_path_info[5] = {
          x: middle_position_mesh.x,
          y: marker.offset,
          z: middle_position_mesh.z
        };
        break;
    }
    this.a_path_info[0] = {
      x: this.start_location.x,
      y: this.start_location.y,
      z: this.start_location.z
    };

    var mat_marker = new BABYLON.StandardMaterial("mat" + type, this.scene);
    mat_marker.specularColor = BABYLON.Color3.Black();
    mat_marker.emissiveColor = BABYLON.Color3.White();
    mat_marker.backFaceCulling = false;
    mat_marker.diffuseTexture = this.a_textures[type];
    mat_marker.diffuseTexture.hasAlpha = true;
    mat_marker.useAlphaFromDiffuseTexture = true;

    marker.material = mat_marker;
    marker.actionManager = new BABYLON.ActionManager(this.scene);

    this.registerEnterExitAction(marker);
    return marker;
  }
  createIconBytype(type, mesh_position) {
    var _this = this;
    var marker = BABYLON.Mesh.CreatePlane("marker_" + type, 0.5, this.scene);
    marker.billboardMode = BABYLON.Mesh.BILLBOARDMODE_Y;

    // this.marker.parent = this.getPrimary().marker_mesh;
    var middle = this.getMiddlePositionArrayMeshes(mesh_position);
    var middle_position_mesh = BABYLON.Mesh.Center(mesh_position);

    marker.position.x = middle_position_mesh.x;
    // marker.position.y = middle_position_mesh.y+2;

    var hight_height_mesh = 0;
    mesh_position.forEach(function(d) {
      d.computeWorldMatrix(true);
      var nh = d.getBoundingInfo().boundingBox.extendSize.scale(2).y;
      if (nh > hight_height_mesh) {
        hight_height_mesh = nh;
      }
    });

    marker.offset = hight_height_mesh + 2 + middle.y;
    marker.position.y = marker.offset;

    marker.position.z = middle_position_mesh.z;
    switch (type) {
      case "environment":
        this.a_path_drone[0] = {
          x: middle_position_mesh.x,
          y: marker.offset,
          z: middle_position_mesh.z
        };
        break;
      case "health":
        this.a_path_drone[1] = {
          x: middle_position_mesh.x,
          y: marker.offset,
          z: middle_position_mesh.z
        };
        break;
      case "virtual":
        this.a_path_drone[2] = {
          x: middle_position_mesh.x,
          y: marker.offset,
          z: middle_position_mesh.z
        };
        break;
      case "transport":
        this.a_path_drone[3] = {
          x: middle_position_mesh.x,
          y: marker.offset,
          z: middle_position_mesh.z
        };
        break;
      case "security":
        this.a_path_drone[4] = {
          x: middle_position_mesh.x,
          y: marker.offset,
          z: middle_position_mesh.z
        };
        break;
    }

    var mat_marker = new BABYLON.StandardMaterial("mat" + type, this.scene);
    mat_marker.specularColor = BABYLON.Color3.Black();
    mat_marker.emissiveColor = BABYLON.Color3.White();
    mat_marker.backFaceCulling = false;
    mat_marker.diffuseTexture = this.a_textures[type];
    mat_marker.diffuseTexture.hasAlpha = true;
    mat_marker.useAlphaFromDiffuseTexture = true;

    marker.material = mat_marker;

    marker.actionManager = new BABYLON.ActionManager(this.scene);

    this.registerEnterExitAction(marker);

    // var keys = [
    //   {
    //     frame: 0,
    //     value: 0.0
    //   }, {
    //     frame: 30,
    //     value: Math.PI
    //   }, {
    //     frame: 60,
    //     value: 0
    //   }
    // ];
    //
    // var animation = new BABYLON.Animation('Marker_rotation_' + type, 'position.y', 30, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
    // animation.setKeys(keys);
    //
    // marker.animations.push(animation);
    // this.scene.beginAnimation(marker, 1 * 2, 60, true);

    // marker.actionManager.registerAction(new BABYLON.InterpolateValueAction(BABYLON.ActionManager.OnPointerOverTrigger, Building.text_mesh, 'material.alpha', 1.0, 300.0, null, true));

    // marker.actionManager.registerAction(new BABYLON.InterpolateValueAction(BABYLON.ActionManager.OnPointerOutTrigger, Building.text_mesh, 'material.alpha', 0.0, 300.0, null, true));
    return marker;
  }

  createParticules(marker, type) {
    var _this = this;

    var particles = new BABYLON.ParticleSystem("Particles_b_" + marker.name, 1000, _this.scene);
    particles.particleTexture = _this.a_textures[type];
    particles.emitter = marker;
    particles.emitRate = 10;

    particles.minEmitBox = new BABYLON.Vector3(-0.2, 0.22, 0);
    particles.maxEmitBox = new BABYLON.Vector3(0.2, -0.22, 0);

    particles.direction1 = new BABYLON.Vector3(-0.15, -0.2, 0);
    particles.direction2 = new BABYLON.Vector3(0.15, -0.5, 0);

    particles.minSize = 0.1;
    particles.maxSize = 0.1;

    particles.color1 = new BABYLON.Color4(1.0, 1.0, 1.0, 1.0);
    particles.color2 = new BABYLON.Color4(1.0, 1.0, 1.0, 1.0);
    particles.colorDead = new BABYLON.Color4(1, 1, 1, 0.0);

    particles.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD;

    particles.start();

    // _thi.highlight = Math.random() + this.offset;

  }
  updateMarker(delta, time) {
    if (this.a_quiz_marker) {
      if (this.a_quiz_marker.length > 0) {
        this.a_quiz_marker.forEach(m => {
          // m.position.y = 1.5 + Math.sin(time + 0) * 0.2;
          // console.log(Math.sin(time + 0) * 0.2);
          m.position.y = m.offset + Math.sin(time + m.offset) * 0.2;
        });
      }
    }
    if (this.a_info_marker) {
      if (this.a_info_marker.length > 0) {
        this.a_info_marker.forEach(m => {
          m.rotation.y = time * 0.2;
        });
      }
    }
    if (this.a_lightColor_material) {
      if (this.a_lightColor_material.length > 0) {
        this.a_lightColor_material.forEach(s => {
          s.setFloat("time", time);
        });

      }
    }

    if (this.meshes) {
      if (this.meshes.length > 0) {
        // this.a_drone[0].position.addInPlace(this.direction.scale(10));
        // this.a_drone[0].position.addInPlace(this.direction);
        // this.a_drone[0].computeWorldMatrix(true);
        this.meshes.forEach(mesh => {
          if (/Trayectoria_2/.test(mesh.name) || /Trayectoria_highest/.test(mesh.name) || /game-health-buildings/.test(mesh.name) || /game-security-buildings/.test(mesh.name) || /game-virtual-singapore-hdb/.test(mesh.name)) {
            if (this.a_drone[0].intersectsMesh(mesh, true)) {
              this.a_drone[0].position = BABYLON.Vector3.Lerp(this.a_drone[0].position, new BABYLON.Vector3(mesh.goodPositionDrone.x, mesh.goodPositionDrone.y, mesh.goodPositionDrone.z), 0.05);
              this.direction = BABYLON.Vector3.Zero();
            } else {
              var last_position = Object.assign({}, this.a_drone[0].position);
              var last_direction = Object.assign({}, this.direction);
              mesh.goodPositionDrone = {};
              mesh.goodPositionDrone.x = last_position.x - last_direction.x * 2;
              mesh.goodPositionDrone.y = last_position.y - last_direction.y * 2;
              mesh.goodPositionDrone.z = last_position.z - last_direction.z * 2;
            }
          }
        });
      }
    }

    if (this.a_drone.length > 0) {
      this.a_drone[0].position = BABYLON.Vector3.Clamp(this.a_drone[0].position, new BABYLON.Vector3(-12, -9.7, -17), new BABYLON.Vector3(15, 0, 10));
      this.a_drone[0].position = BABYLON.Vector3.Clamp(this.a_drone[0].position, new BABYLON.Vector3(-12, -9.7, -11.8), new BABYLON.Vector3(15, 0, 11.9));
    }
  }

  checkElementArray(array, el, key) {
    var value_return = false;
    array.forEach(function(d) {
      var el_compare = d;
      if (key)
        el_compare = d[key];
      if (el_compare === el) {
        value_return = true;
      }
    });
    return value_return;
  }
  getAllAccumulativePositionParents(el, parent_position) {
    if (el.parent !== undefined) {
      if (el.parent.name !== "camera1")
        el.parent.computeWorldMatrix(true);
      parent_position.x += el.parent.position.x;
      parent_position.y += el.parent.position.y;
      parent_position.z += el.parent.position.z;
      this.getAllAccumulativePositionParents(el.parent, parent_position);

    }
    return parent_position;
  }
  getMiddlePositionArrayMeshes(meshes) {
    var low_x = 0,
      low_y = 0,
      low_z = 0;
    var positions_mesh = [];
    meshes.forEach(function(d) {
      low_x += d.position.x;
      if (d.position.y < low_y)
        low_y = d.position.y;
      low_z += d.position.z;
    });

    low_x = low_x / meshes.length;
    low_z = low_z / meshes.length;
    return new BABYLON.Vector3(low_x, low_y, low_z);
  }
  getSky() {
    var skyMaterial = new BABYLON.SkyMaterial("skyMaterial", this.scene);
    skyMaterial.backFaceCulling = false;
    skyMaterial.fogEnabled = false;
    skyMaterial.useSunPosition = true;
    skyMaterial.sunPosition = this.LightDirectional.position;

    // var skybox = BABYLON.Mesh.CreateBox("skyBox", 3000, this.scene);
    var skybox = BABYLON.Mesh.CreateSphere("skyBox", 64, 3000, this.scene);
    skybox.material = skyMaterial;
    skybox.parent = this.camera;
    this.skybox = skybox;
  }

  getSkyCloud() {
    // var skybox = BABYLON.Mesh.CreateBox("skyBox", 1500, this.scene);
    var skybox = BABYLON.Mesh.CreateSphere("skyBox", 64, 3000, this.scene);
    // skybox.position.y = 50;
    var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", this.scene);
    skyboxMaterial.backFaceCulling = false;
    skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("images/skybox/TropicalSunnyDay", this.scene);
    skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
    skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
    skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
    // skyboxMaterial.disableLighting = true;
    skybox.material = skyboxMaterial;
    skybox.layerMask = 2; // 010 in binary
    // this.skybox = skybox;
  }
  createBoxST(mesh) {
    // mesh.getBoundingInfo().world
    mesh.computeWorldMatrix(true);

    var mesh_box_info = mesh.getBoundingInfo();
    var mesh_box_size = mesh_box_info.boundingBox.extendSize.scale(2);
    var center = mesh.getBoundingInfo().boundingSphere.centerWorld;
    // mesh.renderOverlay=true;
    // mesh.overlayColor=BABYLON.Color3.Green();

    // mesh.renderOutline = true;
    // mesh.outlineColor = BABYLON.Color3.Blue();
    // mesh.outlineWidth = 0.5;

    var box = BABYLON.MeshBuilder.CreateBox("boxST", {
      width: mesh_box_size.x,
      height: mesh_box_size.y,
      depth: mesh_box_size.z
    }, this.scene);

    box.position.x = center.x;
    box.position.y = center.y;
    box.position.z = center.z;

    var redWireframeMaterial = new BABYLON.StandardMaterial("lineBox", this.scene);
    redWireframeMaterial.diffuseColor = BABYLON.Color3.Blue();
    redWireframeMaterial.emissiveColor = BABYLON.Color3.Blue();
    redWireframeMaterial.backFaceCulling = true;
    redWireframeMaterial.alpha = 0.5;
    // redWireframeMaterial.wireframe=true;
    // box.showBoundingBox = true;
    box.material = mesh.material;

    box.edgesWidth = 0.2;
    box.edgesColor = mesh.material.diffuseColor.toColor4(0.9);
    box.enableEdgesRendering();
    box.material.alpha = 0.5;
    box.material.backFaceCulling = true;

    mesh.visibility = 0;
  }
  showModal(evt, marker) {
    if (evt) {
      if (evt.source) {
        var mesh_clicked = evt.source;
        var type_modal = "quiz";
        if (/-info/.test(mesh_clicked.name)) {
          type_modal = "info";
        }
        var name_image = mesh_clicked.name.split("marker_")[1].split("-")[0];
        // console.log(mesh_clicked.name);
        // $(".content-card-quiz").append("<div> It is "+type_modal+"</div>");
        // $(".content-card-quiz").animateCss("fadeIn","in");

        //dis(mesh_clicked,type_modal,name_image);
        if (type_modal == "info") {
          $(".clueSection").css("display", "none");
          $(".content-card-info").css("display", "none");
          var infocard = $("#" + type_modal + "_" + name_image);
          $(".clueSection").css("display", "block");
          infocard.css("display", "block");
          if (this.is_guideMode === 0) {
            infocard.delay(10000).fadeOut(function() {
              $(".clueSection").css("display", "none");
            });
          } else {
            var delayVal=this.getDelayInfo(name_image);
            infocard.delay(delayVal).fadeOut(function() {
              $(".clueSection").css("display", "none");
            });
          }
        } else if (type_modal == "quiz") {
          this.cur_intersect_quiz = marker;
          this.cur_intersect_quiz.actionManager.actions = [];
          $(".clueSection").css("display", "none");
          $(".quizSection").css("display", "none");
          $(".content-card-info").css("display", "none");
          $(".content-card-quiz").css("display", "none");
          var quizcard = $("#" + type_modal + "_" + name_image);
          $(".quizSection").css("display", "block");
          $(".quizSection").addClass("upzindex");
          quizcard.css("display", "block");
          if (this.is_guideMode === 1) {
            this.pauseAnimationAndVoice(true);
            // $("#sound-bgm").get(0).volume = this.guideBgmVolume;
            game.bgm.volume=this.guideBgmVolume;
            this.guideIsPause = true;
          } else {
            this.is_automatic = 1;
          }
        }
        // if (this.is_guideMode === 1) {
        //   //remove actions in guide mode, ensure drone and icon only intersect once
        //   mesh_clicked.actionManager.actions.shift();
        // }
      }
    }
  }

  rotateHelice() {
    var _this = this;
    if (this.a_helices.length > 0) {
      this.a_helices.forEach(d => {
        d.computeWorldMatrix(true);
        // var middle_element = d.getBoundingInfo().boundingSphere.centerWorld;
        // console.log(middle_element);
        var middle_element = new BABYLON.Vector3(0, 0, 0);

        // var parent_position = {
        //   x: 0,
        //   y: 0,
        //   z: 0
        // };
        // _this.getAllAccumulativePositionParents(d, parent_position);

        // var mesh=d;
        // mesh.computeWorldMatrix(true);
        // var matrix = mesh.getWorldMatrix().clone();
        // // matrix.invert();
        // var up_local = d.parent.position;
        // var up_global1 = BABYLON.Vector3.TransformCoordinates(up_local, matrix);
        // var parent_position=d.parent.position;
        // var parent_position={x: 0.0001, y: -4.146392373691512, z: 2.0760002218615896}

        var initial_middle = d.initial_middle;
        var parent_position = d.initial_parent_pos;
        // d.computeWorldMatrix(true);
        middle_element.x = initial_middle.x - parent_position.x;
        middle_element.y = initial_middle.y - parent_position.y;
        middle_element.z = initial_middle.z - parent_position.z;

        // console.log(d.parent.parent);

        var rotate_angle = [
          Math.PI / 13,
          Math.PI / 14,
          Math.PI / 15,
          Math.PI / 16
        ];
        d.rotateAroundPivot(middle_element, BABYLON.Axis.Y, rotate_angle[parseInt(Math.random() * 4)]);
      });
    }

  }
  animate_drone_path() {
    var _this = this;
    if (this.is_automatic === 1) {
      var animationDrone = new BABYLON.Animation("automaticAnimation", "position", 20, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
      // animationDrone.enableBlending = true;
      // animationDrone.blendingSpeed = 0.01;

      // var path_frames = [510, 830, 1040, 1390, 1600, 1950, 2100, 2540, 2730, 3100,3300];
      var keys = [];

      keys.push({
        frame: 0,
        value: new BABYLON.Vector3(this.a_path_info[0].x, this.a_path_info[0].y, this.a_path_info[0].z)
      });

      _this.a_path_drone.forEach(function(path, i) {
        keys.push({
          frame: _this.path_frames[i * 2],
          value: new BABYLON.Vector3(_this.a_path_info[i + 1].x, _this.a_path_info[i + 1].y, _this.a_path_info[i + 1].z)
        });
        keys.push({
          frame: _this.path_frames[i * 2 + 1],
          value: new BABYLON.Vector3(path.x, path.y, path.z)
        });
      });
      keys.push({
        frame: _this.path_frames[_this.path_frames.length - 1],
        value: new BABYLON.Vector3(_this.a_path_info[1].x, _this.a_path_info[1].y, _this.a_path_info[1].z)
      });

      animationDrone.setKeys(keys);
      this.a_drone[0].animations.push(animationDrone);
    }
  }
  createControlMobile() {
    var _this = this;
    if (game.joysticks) {
      game.joysticks.destroy();
      game.joysticks = undefined;
    }

    game.joysticks = joysticks.create(game.optionsJoysticks);
    game.joysticks.on("start", function(evt, obj) {
      $(obj.el).addClass("show_area_control");
    });
    game.joysticks.on("move", function(evt, obj) {
      _this.speed = 0.002 * obj.force * 1.3;
    });
    game.joysticks.on("end", function(evt, obj) {
      _this.speed = 0.002;
      _this.rotateSpeed = 0.0007;
      $(obj.el).removeClass("show_area_control");
      if (_this.last_dir_ang !== undefined) {
        evt.keyCode = _this.last_dir_ang;
        _this.onKeyUp(evt);
      }
      if (_this.last_plain_ang !== undefined) {
        evt.keyCode = _this.last_plain_ang;
        _this.onKeyUp(evt);
      }
    });
    game.joysticks.on("dir", function(evt, obj) {
      if (_this.last_dir_ang !== undefined) {
        evt.keyCode = _this.last_dir_ang;
        _this.onKeyUp(evt);
      }
      evt.keyCode = _this.getCodeKeyByDirection(obj.direction.angle);
      _this.last_dir_ang = evt.keyCode;
      _this.onKeyDown(evt);
    });
    game.joysticks.on("plain", function(evt, obj) {
      if (_this.last_plain_ang !== undefined) {
        evt.keyCode = _this.last_plain_ang;
        _this.onKeyUp(evt);
      }
      evt.keyCode = _this.getCodeKeyByDirection(obj.direction.angle);
      _this.last_plain_ang = evt.keyCode;
      _this.onKeyDown(evt);
    });

  }
  getCodeKeyByDirection(dir) {
    var code;
    switch (dir) {
      case "right":
        code = this.keysRight[0];
        break;
      case "left":
        code = this.keysLeft[0];
        break;
      case "up":
        code = this.keysGo[0];
        break;
      case "down":
        code = this.keysBack[0];
        break;
      default:
    }
    return code;
  }
  pointerMouseDown(e) {
    e.preventDefault();
    if (this.scene.isReady() && this.is_guideMode === 0) {
      this.isDragging = true;
      var evt = e
        ? e
        : window.event;
      this.dragInit = {
        x: evt.clientX,
        y: evt.clientY
      };
    }
  }
  pointerMouseMove(e) {
    e.preventDefault();
    if (!this.isDragging)
      return;
    this.clear_keyboard(e);
    this.updateRotationByMouseDrag2(e);
  }
  pointerMouseUp(e) {
    e.preventDefault();
    this.clear_keyboard(e);
    this.isDragging = false;
  }
  clear_keyboard(evt) {
    evt.keyCode = this.keysGo[0];
    this.onKeyUp(evt);
    evt.keyCode = this.keysBack[0];
    this.onKeyUp(evt);
  }
  updateRotationByMouseDrag2(evt) {
    if (this.is_guideMode === 0) {
      var current_drag = {
        x: evt.clientX,
        y: evt.clientY
      };
      if (current_drag.x < this.dragInit.x) {
        this.droneRotation.addInPlace(new BABYLON.Vector3(0, (this.rotateSpeed * -1), 0));
      } else if (current_drag.x > this.dragInit.x) {
        this.droneRotation.addInPlace(new BABYLON.Vector3(0, this.rotateSpeed, 0));
      }

      if (current_drag.y < this.dragInit.y) {
        evt.keyCode = this.keysGo[0];
        this.onKeyDown(evt);
      } else if (current_drag.y > this.dragInit.y) {
        evt.keyCode = this.keysBack[0];
        this.onKeyDown(evt);
      }

      this.dragInit.x = current_drag.x;
    }
  }
  getGuideVrCamera() {
    var _this = this;
    // this.cameraLookPoint.position.y = this.cameraLookPoint.parent.position.y;
    // this.cameraLookPoint.position.y += 0.05;
    this.camera = new BABYLON.VRDeviceOrientationArcRotateCamera("vr_camera", -13, 0.5, 0.1, this.cameraLookPoint, this.scene);
    this.camera.inputs.remove(this.camera.inputs.attached.keyboard);
    // this.camera.inputs.remove(this.camera.inputs.attached.pointers);
    this.camera.inputs.remove(this.camera.inputs.attached.mousewheel);
    this.camera.inputs.remove(this.camera.inputs.attached.gamepad);

    var onOrientationEvent = function(evt) {
      // this._alpha = (- evt.alpha | 0) + 45;
      this._alpha = (-evt.alpha | 0);
      // this._beta = (- evt.beta | 0) + 45;
      this._beta = (-evt.beta | 0);
      if (window.orientation === 90 || window.orientation === 0) {
        // this._gamma = +evt.gamma | 0;
        this._gamma = -evt.gamma | 0;
      } else {
        // this._gamma = +evt.gamma | 0;
        this._gamma = evt.gamma | 0;
      }
      this._dirty = true;
    }

    this.camera.inputs.attached.VRDeviceOrientation._deviceOrientationHandler = onOrientationEvent.bind(this.camera.inputs.attached.VRDeviceOrientation);
    this.camera.inputs.attached.VRDeviceOrientation.attachControl(this.engine.getRenderingCanvas());

    this.camera.lowerRadiusLimit = this.camera.upperRadiusLimit = this.camera.radius;
    this.camera.minZ = 0.01;
    this.camera.attachControl(this.engine.getRenderingCanvas(), true);
    this.scene.activeCamera = this.camera;

    var crosshair = BABYLON.Mesh.CreateDisc('crosshair', 0.01, 64, this.scene);
    crosshair.renderingGroupId = 1;
    crosshair.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
    crosshair.visibility = 0;
    crosshair.position = this.camera.getTarget();
    this.crosshair = crosshair;

    this.btn_correct_texture = new BABYLON.Texture("images/smart-nation/VR/correct-icons-color.png", this.scene);
    this.btn_correct_texture.uAng = 3.15;
    this.btn_incorrect_texture = new BABYLON.Texture("images/smart-nation/VR/incorrect-icons-color.png", this.scene);
    this.btn_incorrect_texture.uAng = 3.15;
  }
  vecToLocal(vector, mesh) {
    var m = mesh.getWorldMatrix();
    var v = BABYLON.Vector3.TransformCoordinates(vector, m);
    return v;
  }
  apply_texture_button_vr(type,index){
    var mesh_choosed=this.result_vr[type];
    if(mesh_choosed){
      if(index+1===parseInt(mesh_choosed.choosed)){
        if(mesh_choosed.answer){
          this.a_btn_vr[index].mesh.material.diffuseTexture=this.btn_correct_texture ;
          this.a_btn_vr[index].mesh.material.emissiveTexture=this.btn_correct_texture ;
        }else{
          this.a_btn_vr[index].mesh.material.diffuseTexture=this.btn_incorrect_texture ;
          this.a_btn_vr[index].mesh.material.emissiveTexture=this.btn_incorrect_texture ;
        }
      }else{
        this.a_btn_vr[index].mesh.material.diffuseTexture = this.a_btn_vr[index].btn_texture;
        this.a_btn_vr[index].mesh.material.emissiveTexture = this.a_btn_vr[index].btn_texture;
      }
    }else{
      this.a_btn_vr[index].mesh.material.diffuseTexture = this.a_btn_vr[index].btn_texture;
      this.a_btn_vr[index].mesh.material.emissiveTexture = this.a_btn_vr[index].btn_texture;
    }
  }
  castRay(mesh_origin) {
    if (mesh_origin) {
      var _this = this;
      var origin = mesh_origin.position;
      var forward = new BABYLON.Vector3(0, 0, 1);
      forward = this.vecToLocal(forward, mesh_origin);
      var direction = forward.subtract(origin);
      direction = BABYLON.Vector3.Normalize(direction);
      var length = 5;
      var ray = new BABYLON.Ray(origin, direction, length);
      if(this.guideIsPause)this.crosshair.visibility = 0.5;
      var hit = this.scene.pickWithRay(ray, function(mesh) {
        var hit_mesh = 0;
        if (_this.a_btn_vr.length > 0) {
          _this.a_btn_vr.forEach((d,num_btn) => {
            if (!_this.result_vr[d.mesh.parent.name.split("-")[0]]) {
              if (d.mesh == mesh) {
                hit_mesh = 1;
              } else {
                d.mesh.material.diffuseTexture = d.btn_texture;
                d.mesh.material.emissiveTexture = d.btn_texture;

                d.mesh.scaling = new BABYLON.Vector3(1, 1, 1);
                _this.picked_mesh_vr = 0;
              }
            }else{
              _this.apply_texture_button_vr(d.mesh.parent.name.split("-")[0],num_btn);
            }
          });
        }
        if(_this.resultPlane===mesh && _this.cast_ray_res===1){
          hit_mesh=1;
        }
        if(hit_mesh===0){
          _this.picked_mesh_vr = 0;
        }
        return hit_mesh;
      });
      if (hit.pickedMesh) {
        if(_this.guideIsPause)_this.crosshair.visibility = 0;
        if(hit.pickedMesh!=_this.resultPlane){
        _this.a_btn_vr.forEach(d => {
            if (d.mesh == hit.pickedMesh) {
              hit.pickedMesh.material.diffuseTexture = d.hover_texture;
              hit.pickedMesh.material.emissiveTexture = d.hover_texture;
              hit.pickedMesh.scaling = new BABYLON.Vector3(1.05, 1.05, 1.05);
              _this.picked_mesh_vr = hit.pickedMesh;
            }
          });
        }else if(_this.cast_ray_res===1){
          _this.picked_mesh_vr=hit.pickedMesh;
        }
      }
    }
  }
  create_button_VrQ(indexLetter, i) {
    var btnMaterial = new BABYLON.StandardMaterial("btnDisc-material-" + indexLetter[i], this.scene);
    var btn_vr_disc = {};
    btn_vr_disc.mesh = BABYLON.Mesh.CreateDisc('btn-vr-hover' + (i + 1), 0.07, 64, this.scene);
    btn_vr_disc.btn_texture = new BABYLON.Texture("images/smart-nation/VR/option-" + indexLetter[i] + ".png", this.scene);
    btn_vr_disc.btn_texture.uAng = 3.15;
    btn_vr_disc.hover_texture = new BABYLON.Texture("images/smart-nation/VR/option-" + indexLetter[i] + "-hover.png", this.scene);
    btn_vr_disc.hover_texture.uAng = 3.15;
    btnMaterial.diffuseTexture = btn_vr_disc.btn_texture;
    btnMaterial.emissiveTexture = btn_vr_disc.btn_texture;
    btn_vr_disc.mesh.material = btnMaterial;

    // btn_vr_disc.mesh.renderingGroupId = 1;
    btn_vr_disc.mesh.position.y = -0.16;
    btn_vr_disc.mesh.position.x = -0.02;
    btn_vr_disc.mesh.position.z = -0.001;
    btn_vr_disc.mesh.parent = this.ansPlanes[i];
    this.a_btn_vr.push(btn_vr_disc);
  }
  hideDrone(){
    var _this=this;
    this.a_drone.forEach(d=>{
      _this.hideVisibility(d);
    });
  }
  showDrone(){
    var _this=this;
    this.a_drone.forEach(d=>{
      _this.showVisibility(d);
    });
  }
  execute_selection_answer() {
    var _this=this;
    if(game.picked_mesh_vr.name==="resultPlane"){
        // _this.restartGameVr();
       location.reload();
        return;
    }
    if (this.picked_mesh_vr && this.data_csv) {
      var a_name = this.picked_mesh_vr.parent.name.split("-");
      var type_choosed = a_name[0];
      var number_choosed = a_name[a_name.length - 1];
      if (!this.result_vr[type_choosed]) {
        this.result_vr[type_choosed] = {};
        if (this.verify_answer(type_choosed, number_choosed) === 1) {
          this.result_vr.correct++;
          this.result_vr[type_choosed].answer = 1;
        } else {
          this.result_vr.incorrect++;
          this.result_vr[type_choosed].answer = 0;
        }
        this.result_vr[type_choosed].choosed = number_choosed;
        this.apply_texture_button_vr(type_choosed,parseInt(number_choosed-1));
        var number_q=d3.keys(_this.result_vr).length;
        d3.timeout(d=>{
          _this.hideVRQuizPlane();
          d3.timeout(d=>{
            // _this.crosshair.visibility = 0;
            _this.guideIsPause = false;
            if(number_q!==7){
              _this.hideVisibility(_this.crosshair);
              _this.pauseAnimationAndVoice(false);
              _this.showDrone();
            }else{
              _this.showresultpageVR(_this.result_vr.correct);
              _this.cast_ray_res=1;
            }
          },500);
        },500);
      }
    }
  }
  verify_answer(type_choosed, number_choosed) {
    var value_verify = 0;
    this.data_csv.forEach(d => {
      if (d.topic_id.trim() === type_choosed.trim()) {
        if (parseInt(d.correctoption) === parseInt(number_choosed))
          value_verify = 1;
        }
      });
    return value_verify;
  }
  getDelayInfo(name_image){
    var delayVal = 10000;
    switch (name_image) {
      case "environment":
        delayVal = 10000;
        break;
      case "health":
        delayVal = 11000;
        break;
      case "virtual":
        delayVal = 11000;
        break;
      case "transport":
        delayVal = 21000;
        break;
      case "security":
        delayVal = 18000;
        break;
    }
    return delayVal;
  }
  hideVisibility(mesh){
    BABYLON.Animation.CreateAndStartAnimation("hide_"+mesh.name,mesh,"visibility",24,30,0.8,0,BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
  }
  showVisibility(mesh,opacity){
    if(opacity===undefined)opacity=1;
    BABYLON.Animation.CreateAndStartAnimation("show_"+mesh.name,mesh,"visibility",24,30,0.8,opacity,BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
  }
  hideVRQuizPlane(){
    var _this=this;
    this.ansPlanes.forEach(d=>{
      _this.hideVisibility(d);
      _this.hideVisibility(d.getChildMeshes()[0]);
    });
    _this.hideVisibility(_this.quesPlane);
  }
  showButtonsVr(){
    var _this=this;
    this.a_btn_vr.forEach(d=>{
      _this.showVisibility(d.mesh);
    });
  }
  showModal_vr(evt,marker){
    if (evt) {
      if (evt.source) {
        var mesh_clicked = evt.source;
        var type_modal = "quiz";
        if (/-info/.test(mesh_clicked.name)) {
          type_modal = "info";
        }
        var name_image = mesh_clicked.name.split("marker_")[1].split("-")[0];
        if(type_modal==="quiz")
        // {
        //   this.showVRInfoPlane(name_image);
        //   var delayVal=this.getDelayInfo(name_image);
        //   d3.timeout(d=>{
        //     this.hideVRInfoPlane();
        //   },delayVal);
        // }else
        {
          // $("#sound-bgm").get(0).volume = this.guideBgmVolume;
          this.bgm.volume=this.guideBgmVolume;
          this.pauseAnimationAndVoice(true);
          this.hideDrone();
          d3.timeout(d=>{
            this.destroyIcons(name_image);
            this.showVRQuizPlanes(name_image);
            this.showButtonsVr();
            this.guideIsPause = true;
            this.crosshair.visibility = 0.5;
          },500);
        }
      }
    }
  }
  destroyIcons(type) {
    var _this=this;
    _this.a_quiz_marker.forEach(function(quiz_mark) {
      if (quiz_mark.name.split("marker_")[1].split("-")[0] === type) {
        quiz_mark.name = "d_"+quiz_mark.name;
        // quiz_mark = null;
        quiz_mark.dispose();
        // game.quiz_arrows[type].dispose();
        _this.a_info_marker.forEach(function(info_mark) {
          if (info_mark.name.split("marker_")[1].split("-")[0] === type) {
            info_mark.dispose();
            _this.lineDotsGroup.forEach(function(lineDots) {
              if (lineDots.name.split("lineHospitalinfo-")[1].split("-")[0] === type) {
                lineDots.dispose();
                _this.quizBeamGroup.forEach(function(quizBeam) {
                  if (quizBeam.name.split("Building_beams_game-")[1].split("-")[0] === type) {
                    quizBeam.dispose();
                    return;
                  }
                });
              }
            });
          }
        });
      }
    });
  }
  deleteIcons(){
    var _this=this;
    this.data_csv.forEach(d=>{
      _this.destroyIcons(d.topic_id.trim());
    })

  }
  restartGameVr(){
    this.picked_mesh_vr=0;
    this.result_vr={
      correct:0,
      incorrect:0
    };
    this.cast_ray_res=0;
    this.guideIsPause=false;
    this.hideVisibility(this.resultPlane);
    this.a_animatable.reset();
    this.deleteIcons();
    this.showDrone();
    this.narrativeSound.currentTime=0;
    this.pauseAnimationAndVoice(false);
    this.hideVRQuizPlane();
    this.hideVRInfoPlane();
    this.createIcons(); //we need to clean the textures
  }
}
