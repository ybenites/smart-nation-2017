import * as BABYLON from "babylonjs";
global.copy_baby=BABYLON;
global.BABYLON=BABYLON;

BABYLON.Mesh.prototype.scaleFromPivot = function(pivotPoint, sx, sy, sz) {
  var _sx = sx / this.scaling.x;
  var _sy = sy / this.scaling.y;
  var _sz = sz / this.scaling.z;
  this.scaling = new BABYLON.Vector3(sx, sy, sz);
  this.position = new BABYLON.Vector3(pivotPoint.x + _sx * (this.position.x - pivotPoint.x), pivotPoint.y + _sy * (this.position.y - pivotPoint.y), pivotPoint.z + _sz * (this.position.z - pivotPoint.z));
}

BABYLON.Mesh.prototype.rotateAroundPivot = function(pivotPoint, axis, angle) {
    if(!this._rotationQuaternion) {
        this._rq = BABYLON.Quaternion.RotationYawPitchRoll(this.rotation.y, this.rotation.x, this.rotation.z);
    }
    var _p = new BABYLON.Quaternion(this.position.x - pivotPoint.x, this.position.y - pivotPoint.y, this.position.z - pivotPoint.z, 0);
    axis.normalize();
    var _q = BABYLON.Quaternion.RotationAxis(axis,angle);  //form quaternion rotation
    var _qinv = BABYLON.Quaternion.Inverse(_q);
    var _pdash = _q.multiply(_p).multiply(_qinv);
    this.position = new BABYLON.Vector3(pivotPoint.x + _pdash.x, pivotPoint.y + _pdash.y, pivotPoint.z + _pdash.z);
    this.rotationQuaternion = this._rq.multiply(_q);
    this._rq = this.rotationQuaternion;
}

var joysticks=require('nipplejs');
global.joysticks=joysticks;

import "./libraries/sound";

// require('canvas2d');
// require("./libraries/babylon.brickProceduralTexture");
// require("./libraries/babylon.gridMaterial");
// require("./libraries/babylon.fireMaterial");
// require("./libraries/babylon.2.5.joy");

// import CANNON from 'cannon';
// global.CANNON=CANNON;
