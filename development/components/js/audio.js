export default class STAudio{
  constructor(instance) {
    if(instance)this.instance=instance;
  }
  get source(){
    return this.instance;
  }
  set source(instance){
    this.instance=instance;
  }
  set volume(volume){
    this.instance.setVolume(volume);
  }

  play(){
    var _this=this;
    if(this.instance){
      if(this.instance.position<0)this.instance.position=0;
      this.instance.setPaused(false);
    }else{
      d3.timeout(d=>{
        _this.play();
      },100);
    }
  }
  pause(){
    this.instance.setPaused(true);
  }
  get currentTime(){
    return this.instance.position;
  }
  set currentTime(time){
    this.instance.position=time;
  }
  get ended(){
    return this.instance.position===this.instance.getDuration();
  }
  get paused(){
    return this.instance.paused;
  }
  get muted(){
    return this.instance.muted
  }
  set muted(value){
    this.instance.muted=value;
  }
}
